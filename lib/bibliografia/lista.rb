#clase que representa la lista enlazada
Node=Struct.new(:value,:next)
class Lista
    attr_reader :cabeza,:n_elementos
    def initialize()
        @cabeza=Node.new(nil,nil)
        @n_elementos=0          
    end
    def inserta(nodo)
        nodo.each do |n|
            if(@cabeza.value==nil)
                @cabeza=n
                @cabeza.next=nil
                
            else
                a_cabeza=@cabeza
                while a_cabeza.next!=nil do
                    
                    a_cabeza=a_cabeza.next
                end
                a_cabeza.next=n
                
            end
            @n_elementos=@n_elementos+1
       end
    end
    def extrae
        a_cabeza=@cabeza
        @cabeza=@cabeza.next
        @n_elementos=@n_elementos-1
        a_cabeza
    end
    def empty?
        if(@cabeza.value==nil)
            true
        else
            false
        end
    end
      
end