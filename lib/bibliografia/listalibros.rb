require_relative "lista.rb"
class Listalibros < Lista
    
     attr_reader :autor, :isbn, :titulo, :fecha_publicacion, :serie, :editorial, :num_edicion
   def initialize(autor,isbn,titulo,fecha,editorial,edicion)

      #variables de instancia
		@autor=autor
		@isbn=isbn
		@titulo=titulo
		@fecha_serie=fecha
		@editorial=editorial
		@num_edicion=edicion
   end

  #metodo para insertar la serie del libro
   def set_serie(serie)
   	 	@serie=serie
   	 
   end
   
   def to_s
       "#{self.class}: #{@autor}, #{@isbn},#{@titulo},#{@fecha_serie},#{@editorial},#{@num_edicion},#{@serie}"

   end
end