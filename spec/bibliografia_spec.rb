require "spec_helper"
require "./lib/bibliografia/biblio.rb"
require "./lib/bibliografia/lista.rb"
require "./lib/bibliografia/listalibros.rb"
  describe Biblioteca do
   before :each do
   	 @bil=Biblioteca.new(["pepe","juan"],["123","456","999"],"Programacion en Ruby","12/5/15","santillana","4 edicion")
	end
	

		 it "No puede haber ningún autor" do
    		     @bil.autor.length.should_not be 0
    		   
    		 end
    
		 it "El libro debe tener un titulo" do
    		  
    		     @bil.titulo != ""
    		 end
	
		 it "Debe haber o no una serie" do
    		   
    		    @bil.serie
    		 end
	
		 it "No puede haber ningún ISBN" do
    		   
    		     @bil.isbn.length.should_not be 0
    		 end
   
		 it "Debe haber una editorial" do
    		   
    		     @bil.editorial != ""
    		 end
	
		 it "Debe haber una numero de edicion" do
    		   
    		  
    		     @bil.num_edicion != ""
    		 end
	
		 it "Debe haber una fecha de publicacion" do
    		  
    		  
    		     @bil.fecha_publicacion != ""
    		 end
    	it "insertada la serie" do
    		@bil.set_serie("Programacion")
    	end

    	describe Node do
    		before:all do
    			@nod=Node.new("pepe",nil)
    		end
    			it "Debe existir un nodo de la lista con sus datos y su siguiente" do
	   	 			expect(@nod.value).not_to eq(nil)
	   	    end	
        end
        describe Lista do
    		before:all do
    			@nod1=Node.new("pepe",nil)
    			@nod2=Node.new("juan",nil)
    			@nod3=Node.new("antonio",nil)
    			@list=Lista.new()
    			@list.inserta([@nod1,@nod2,@nod3])
    		end
    		it "Existe una lista vacia" do
		   	   expect(Lista.new().empty?).to eq(true)	
	   	    end
	   	     it "Se puede insertar un elemento" do
	   	    		@list2=Lista.new()
	   	    		@list2.inserta([@nod1])
	   	    end
	   	    it "Se puede insertar varios elementos" do
	   	    		@list3=Lista.new()
	   	    		@list3.inserta([@nod1,@nod2])
	   	    end
	   	     it "debe existir una Lista con su cabeza" do
		   	   expect(@list.cabeza).to eq(@nod1)	
		   	   
	   	    end
	   	    it "Se extrae el primer elemento de la lista"do
	   	    	  @list.extrae	
	   	    end
	   	end
			describe Listalibros do
	   		before:all do
	   			
	   			@libro1=Listalibros.new(["Dave Thomas, Andy Hunt, Chad Fowler"],[" ISBN-13: 978-1937785499.
				ISBN-10: 1937785491."],"Programming Ruby 1.9 & 2.0: The Pragmatic Programmers’
				Guide", "(July 7, 2013)","Pragmatic Bookshelf;"," 4 edition")
				@libro1.set_serie("Serie 2")
				
				@libro2=Listalibros.new(["Scott Chacon"],[" ISBN-13: 978-
				1430218333. ISBN-10: 1430218339."],"Pro Git 2009th Edition", "(August 27, 2009)","(Pro). Apress"," 2009 edition")
				
				
				@libro3=Listalibros.new(["David Flanagan, Yukihiro Matsumoto"],[" ISBN-13: 978-0596516178"],
				"The Ruby Programming Language","(February
				4, 2008)","O’Reilly Media;"," 1 edition.")
				@libro3.set_serie("Serie 6")
				
				@libro4=Listalibros.new(["David Chelimsky, Dave Astels, Bryan Helmkamp, Dan North, Zach Dennis, Aslak Hellesoy."],
				["ISBN-10: 1934356379. ISBN-13: 978-1934356371."],
				" The RSpecBook: Behaviour Driven Development with RSpec,Cucumber, and Friends (The Facets of Ruby)Cucumber, and Friends (The Facets of Ruby)",
				"(December 25, 2010)","Pragmatic Bookshelf","1 edition")
				@libro4.set_serie("Serie 7")
				
				@libro5=Listalibros.new(["Richard E. Silverman"],
				[" ISBN-10: 1449325866.ISBN-13: 978-1449325862"],"Git Pocket Guide O’Reilly Media",
				"(August 2, 2013)","Pragmatic Bookshelf"," 1 edition")
				    
	   		end
	   			it "existe una lista de libros" do
		   	   		@nod1=Node.new(@libro1.to_s,nil)
		   	   		@nod2=Node.new(@libro2.to_s,nil)
		   	   		@nod3=Node.new(@libro3.to_s,nil)
		   	   		@nod4=Node.new(@libro4.to_s,nil)
		   	   		@nod5=Node.new(@libro5.to_s,nil)
					@list=Lista.new()
					@list.inserta([@nod1,@nod2,@nod3,@nod4,@nod5])
						
	   	    	end
	   		
		end
end